from __future__ import division
import numpy as np
from hmmlearn import hmm

states = ["Sunny", "Rainy", "Foggy"]
n_states = len(states)

observations = ["U", "N"]
n_observations = len(observations)

start_probability = np.array([1/3, 1/3, 1/3])

transition_probability = np.array([
    [0.8, 0.05, 0.15],
    [0.2, 0.6, 0.2],
    [0.2, 0.3, 0.5]
])

emission_probability = np.array([
    [0.1, 0.9],
    [0.8, 0.2],
    [0.3, 0.7]
])

model = hmm.MultinomialHMM(n_components=n_states)
model.startprob_ = start_probability
model.transmat_ = transition_probability
model.emissionprob_ = emission_probability

# predict a sequence of hidden states based on visible states
phi = np.array([[1, 1, 1]]).T

#model = model.fit(phi)
logprob, psi = model.decode(phi, algorithm="viterbi")
print("Observation Sequence:", ", ".join(map(lambda x: observations[x], phi.T[0])))
print("The Most Probable States:", ", ".join(map(lambda x: states[x], psi)))
print("Probability: ", np.exp(logprob))